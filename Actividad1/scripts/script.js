const numero1 = document.querySelector("#numero1");
const numero2 = document.querySelector("#numero2");
const resultado = document.querySelector(".result");

const formulario = document.querySelector(".formulario");

console.log(numero1);

const datos = {
  numero1: "",
  numero2: "",
};

numero1.addEventListener("input", leerTexto);
numero2.addEventListener("input", leerTexto);

function leerTexto(e) {
  datos[e.target.id] = parseInt(e.target.value);
}

formulario.addEventListener("submit", function (e) {
  e.preventDefault();

  const { numero1, numero2 } = datos;

  numeroRandom(numero1, numero2);
});

function numeroRandom(min, max) {
  const nr = Math.floor(Math.random() * (max - min + 1)) + min;
  resultado.textContent = nr;
}

function sumar(n1, n2) {
  const su = n1 + n2;

  resultado.textContent = su;
}
