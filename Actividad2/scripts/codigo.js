var sumar = document.getElementById("sumar");
var restar = document.getElementById("restar");
var multiplicar = document.getElementById("multiplicar");
var dividir = document.getElementById("dividir");

const alert = document.querySelector(".alert");
var resultado = document.getElementById("resultado");

var numero1 = document.getElementById("input-uno");
var numero2 = document.getElementById("input-dos");

sumar.addEventListener("click", function () {
  var n1 = numero1.value;
  var n2 = numero2.value;

  if (n2 === null || n1 === null || n2 === "" || n1 === "") {
    mostrarAlerta("Todos los campos son obligatorios", "error");
  } else {
    var n11 = Number(numero1.value);
    var n22 = Number(numero2.value);

    if (Number.isInteger(n22) && Number.isInteger(n11)) {
      mostrarAlerta("Sumado", "correcto");
      suma(n1, n2);
    } else {
      mostrarAlerta("Asegurese de que los numeros son Enteros", "error");
    }
  }
});

restar.addEventListener("click", function () {
  var n1 = numero1.value;
  var n2 = numero2.value;
  if (n2 === null || n1 === null || n2 === "" || n1 === "") {
    mostrarAlerta("Todos los campos son obligatorios", "error");
  } else {
    var n11 = Number(numero1.value);
    var n22 = Number(numero2.value);

    if (Number.isInteger(n22) && Number.isInteger(n11)) {
      mostrarAlerta("Restado", "correcto");
      resta(n1, n2);
    } else {
      mostrarAlerta("Asegurese de que los numeros son Enteros", "error");
    }
  }
});

multiplicar.addEventListener("click", function () {
  var n1 = numero1.value;
  var n2 = numero2.value;
  if (n2 === null || n1 === null || n2 === "" || n1 === "") {
    mostrarAlerta("Todos los campos son obligatorios", "error");
  } else {
    var n11 = Number(numero1.value);
    var n22 = Number(numero2.value);

    if (Number.isInteger(n22) && Number.isInteger(n11)) {
      mostrarAlerta("Multiplicado", "correcto");
      multi(n1, n2);
    } else {
      mostrarAlerta("Asegurese de que los numeros son Enteros", "error");
    }
  }
});

dividir.addEventListener("click", function () {
  var n1 = numero1.value;
  var n2 = numero2.value;

  if (n2 === null || n1 === null || n2 === "" || n1 === "") {
    mostrarAlerta("Todos los campos son obligatorios", "error");
  } else {
    if (n2 === "0") {
      mostrarAlerta("No se puede Dividir por 0", "error");
    } else {
      var n11 = Number(numero1.value);
      var n22 = Number(numero2.value);
      if (Number.isInteger(n22) && Number.isInteger(n11)) {
        mostrarAlerta("Dividido", "correcto");
        divi(n1, n2);
      } else {
        mostrarAlerta("Asegurese de que los numeros son Enteros", "error");
      }
    }
  }
});

function suma(n1, n2) {
  resultado.textContent = parseInt(n1) + parseInt(n2);
}

function resta(n1, n2) {
  resultado.textContent = parseInt(n1) - parseInt(n2);
}

function multi(n1, n2) {
  resultado.textContent = parseInt(n1) * parseInt(n2);
}

function divi(n1, n2) {
  resultado.textContent = parseInt(n1) / parseInt(n2);
}

function mostrarAlerta(mensaje, estilo) {
  const alerta = document.createElement("p");
  alerta.classList.add(estilo);
  alerta.textContent = mensaje;

  alert.appendChild(alerta);

  setTimeout(() => {
    alerta.remove();
  }, 2000);
}
